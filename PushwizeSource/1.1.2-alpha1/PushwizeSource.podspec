Pod::Spec.new do |s|
    s.name              = 'PushwizeSource'
    s.version           = '1.1.2-alpha1'
    s.summary           = 'Pushwize iOS SDK'
    s.homepage          = 'https://mondriaan.com/'
    s.author            = { 'Levente Dimény' => 'levente.dimeny@mondriaan.com' }
    s.license           = 'Proprietary'
    s.platform          = :ios
    s.source            = { :git => "ssh://git@source.mondriaan.com:7999/pw/pushwize-ios-sdk.git", :tag => s.version.to_s }
    s.ios.deployment_target = '9.0'
    s.module_name = 'Pushwize'
    s.frameworks = 'Foundation', 'UIKit'

    s.subspec 'Common' do |common|
      common.source_files = 'Pushwize/BuildInfo/PushwizeBuildInfo.swift', 'Pushwize/Inbox/PushwizeInboxItem.swift', 'Pushwize/Encryption/*.swift', 'Pushwize/Encryption/CryptoSwift/*.swift', 'Pushwize/Encryption/CryptoSwift/AEAD/*.swift', 'Pushwize/Encryption/CryptoSwift/Foundation/*.swift', 'Pushwize/Encryption/CryptoSwift/PKCS/*.swift', 'Pushwize/Encryption/CryptoSwift/BlockMode/*.swift', 'Pushwize/Statistics/*.swift', 'Pushwize/Configuration/PushwizeRemoteConfiguration.swift', 'Pushwize/Common/*.swift', 'Pushwize/Sync/PushwizeNotificationType.swift', 'Pushwize/Sync/PushwizeSyncPayload.swift', 'Pushwize/Sync/PushwizeRegistration.swift', 'Pushwize/NotificationServiceExtension/PushwizeMessage.swift', 'Pushwize/NotificationServiceExtension/PushwizeBadgeHandler.swift', 'Pushwize/NotificationServiceExtension/PushwizeMessageAttachment.swift', 'Pushwize/Location/PushwizeGeoTagPrivacy.swift', 'Pushwize/Location/PushwizeLocation.swift', 'Pushwize/Location/PushwizeLocationScope.swift', 'Pushwize/API/*.swift'
      common.frameworks = 'Security'
    end

    s.subspec 'Core' do |core|
      core.source_files = 'Pushwize/BuildInfo/PushwizeBuildInfoApiCall.swift', 'Pushwize/BuildInfo/PushwizeBuildInfoApiCallProtocol.swift', 'Pushwize/BuildInfo/PushwizeBuildInfoApiCallHelper.swift', 'Pushwize/BuildInfo/PushwizeBuildInfoApiCallHelperProtocol.swift', 'Pushwize/Inbox/UI/*.swift', 'Pushwize/Inbox/PushwizeInboxApiCall.swift', 'Pushwize/Inbox/PushwizeInboxApiCallProtocol.swift', 'Pushwize/Inbox/PushwizeInboxApiCallHelper.swift', 'Pushwize/Inbox/PushwizeInboxApiCallHelperProtocol.swift', 'Pushwize/Inbox/PushwizeInboxZipUtility.swift', 'Pushwize/Inbox/PushwizeInboxZipUtilityProtocol.swift', 'Pushwize/Inbox/UIApplication+TopViewController.swift', 'Pushwize/Inbox/PushwizeInboxWebView.swift', 'Pushwize/Configuration/PushwizeRemoteConfigurationApiCall.swift', 'Pushwize/Configuration/PushwizeRemoteConfigurationApiCallProtocol.swift', 'Pushwize/Configuration/PushwizeRemoteConfigurationApiCallHelper.swift', 'Pushwize/Configuration/PushwizeRemoteConfigurationApiCallHelperProtocol.swift', 'Pushwize/Engine/*.swift', 'Pushwize/Sync/PushwizeSyncApiCallHelper.swift', 'Pushwize/Sync/PushwizeSyncApiCallHelperProtocol.swift', 'Pushwize/Sync/PushwizeSyncApiCall.swift', 'Pushwize/Sync/PushwizeSyncApiCallProtocol.swift', 'Pushwize/Location/PushwizeLocationApiCallHelper.swift', 'Pushwize/Location/PushwizeLocationApiCallHelperProtocol.swift', 'Pushwize/Location/PushwizeLocationApiCall.swift', 'Pushwize/Location/PushwizeLocationApiCallProtocol.swift', 'Pushwize/Ping/*.swift'
      core.resources = 'Pushwize/Inbox/UI/*.xib'
      core.frameworks = 'Foundation', 'UIKit'
      core.dependency 'ZIPFoundation', '~> 0.9.11'
      core.dependency 'PushwizeSource/Common'
    end

    s.subspec 'NotificationService' do |notification_service|
      notification_service.source_files = 'Pushwize/NotificationServiceExtension/PushwizeNotificationServiceExtension.swift'
      notification_service.frameworks = 'UserNotifications'
      notification_service.dependency 'PushwizeSource/Common'
    end
end
